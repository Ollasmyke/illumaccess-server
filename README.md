# Illumaccess

This Project, Illumaccess: A Smart Learning Environment for less-privileged children.

Developed by ollasmyke@gmail.com
- Version 0.0.0: 22nd July 2020.

The project was auto created by Express Generator, for the Server side.
MongoDB for the Database connection.

In the project directory, you can run:
### `npm start`
Runs the app in the development mode.<br>
Open [http://localhost:8006](http://localhost:8006) to view it in the browser.<br/>

Swagger UI for API documentation.
### `npm test`
Runs the test and code coverage in the development mode.<br>

Git Flow Workflow as Branching Model

This is the Back End version for this App.
