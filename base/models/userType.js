const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

// User Types Schema
const userTypeSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        uniqueCaseInsensitive: true,
        dropDups: true,
        trim: true,
    },
    permissions: {
        type: Object,
        default: {}
    }
}, {
    timestamps: true
});


userTypeSchema.plugin(uniqueValidator);

const userTypes = mongoose.model('UserTypes', userTypeSchema);
module.exports = userTypes;
