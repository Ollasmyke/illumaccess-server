const express = require('express');
const router = express.Router();
const users = require('../base/controllers/Users.controller');
const authService = require('../base/services/AuthService');

// router.get('/', (req, res, next) => {authService.checkAuth(req, res, next)}, (req, res) => {
//   users.getAllUsers(req, res);
// });

router.get('/', (req, res) => {
  users.getAllUsers(req, res);
});

router.post('/', (req, res) => {
  users.createUser(req, res);
});

router.get('/:id', (req, res) => {
  users.getUserById(req, res);
});

router.get('/getUserByGroupId/:id', (req, res) => {
  users.getUserByGroupId(req, res);
});

router.get('/toggleUser/:id', (req, res) => {
  users.toggleUser(req, res);
});

router.patch('/:id',  (req, res, next) => {
  users.updateUser(req, res);
});

router.patch('/changePassword',  (req, res, next) => {
  users.changePassword(req, res);
});

router.patch('forgotPassword/:id',  (req, res, next) => {
  users.forgotPassword(req, res);
});

router.delete('/:id',  (req, res, next) => {
  users.deleteUser(req, res);
});

module.exports = router;
