const config = require('../../constants');
const User = require('../models/user');
const Token = require('../models/token');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const AuthService = require("../services/AuthService");

// For User Login and Log out Authentication
const AuthenticationController = {

    login: (req, res) => {
        const {email, password} = req.body;
        try {
            User.findOne({email: email})
                .populate('userType')
                .exec((err, user) => {
                    if (err || !user) {
                        res.status(401).json({
                            message: 'Error occurred, email address or password is invalid',
                            error: err
                        });
                    } else {
                        bcrypt.compare(password, user.password, (err, response) => {
                            if (err || !response) {
                                res.status(401).json({
                                    message: 'FAILURE, Incorrect user email address or password!',
                                    error: err
                                });
                            } else {
                                if (user.status === config.STATUS_OPTIONS.DISABLED) {
                                    res.status(401).json({
                                        message: 'Account temporarily disabled for security reasons, contact System Administrators',
                                        error: err
                                    });
                                } else {
                                    const randomNumber = AuthService.nanoidGenerator();
                                    const userObject = {
                                        email: user.email,
                                        firstName: user.firstName,
                                        lastName: user.lastName,
                                        fullName: user.firstName + ' ' + user.lastName,
                                        status: user.status,
                                        id: user._id,
                                        userType: user.userType.name,
                                        token: randomNumber
                                    };
                                    Token.create({token: randomNumber, userId: user._id}, () => {
                                        jwt.sign(userObject, process.env.JWT_KEY, {expiresIn: '7d'}, (err, token) => {
                                            res.status(201).json({
                                                message: 'Login success!',
                                                data: {token}
                                            });
                                        })

                                    });
                                }
                            }
                        });
                    }

                });


        } catch (e) {
            res.status(400).json({
                message: 'wrong password',
                error: e
            })

        }
    },

    logout: (req, res) => {
        const id = AuthService.getValue(req, 'id');
        Token.remove({userId: id}, (err) => {
            if (err) {
                res.status(400).json({
                    message: 'Error trying to logout!'
                });

            } else {
                res.status(200).json({
                    message: 'Logout successful'
                });

            }

        })
    }


};

module.exports = AuthenticationController;
