const Type = require('../models/userType');

// User Types Controller
const UserTypeController = {
    getAllUserTypes: (req, res) => {
        Type.find({}, (err, data) => {
            if (err) {
                res.status(400).json({
                    message: 'Error Occurred fetching all user types',
                    error: err
                });
            } else {
                res.status(200).json({
                    data: data,
                    message: 'All User Types fetched successfully'
                });
            }
        });

    },

    createNewUserType: (req, res) => {
        Type.findOne({name: req.body.name}, (error, data) => {
            if (error) {
                res.status(400).json({
                    message: 'Error occurred, creating new User type. Contact Server Administrator',
                    error: error
                });
            } else {
                if (data) {
                    res.status(400).json({
                        message: 'User type already exists'});
                } else {
                    const type = req.body;
                    Type.create(type, (err, data) => {
                        if (err) {
                            res.status(400).json({
                                message: err.errors[Object.keys(err.errors)[0]].message || 'Error occurred creating user type',
                                error: error})
                        } else {
                            res.status(201).json({
                                message: 'User type created Successfully. Received JSON object to save',
                                userType: data});
                        }
                    })
                }
            }
        });

    },

    getUserTypeById: (req, res) => {
        const id = req.params.id;
        Type.findById(id, (err, data) => {
                if (err) {
                    res.status(404).json({
                        message: 'Data resource unavailable, wrong ID'});
                } else {
                    res.status(200).json({
                        message: 'Data found',
                        data: data});
                }
            })

    },

    updateUserType: (req, res) => {
        const id = req.params.id;
        Type.findByIdAndUpdate( id, {$set: req.body}, {new: true}, (err, data) => {
            if (err) {
                res.status(404).json({
                    message: 'Data resource unavailable, wrong ID',
                    error: err});
            } else {
                res.status(200).json({
                    message: 'User type found and updated successfully',
                    data: data});
            }
        })
    },

    deleteUserType: (req, res) => {
        const id = req.params.id;
        Type.findByIdAndRemove(id, (err, data) => {
            if (err) {
                res.status(404).json({
                    message: 'Data resource unavailable, wrong ID',
                    error: err});
            } else {
                res.status(200).json({
                    message: 'User type deleted',
                    data: data});
            }
        });
    }
};

module.exports = UserTypeController;
