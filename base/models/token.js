const mongoose = require('mongoose');
const moment = require('moment');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

// Create a token schema
const tokenSchema = new Schema({
    token: {
        type: String,
        required: true
    },
    expired: {
        type: Boolean,
        default: false
    },
    expireDate: {
        type: Date,
        default: moment().add(6, "hours")
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users',
        required: true
    }

}, {
    timestamps: true
});


tokenSchema.plugin(uniqueValidator);
const Tokens = mongoose.model('Token', tokenSchema);

module.exports = Tokens;
