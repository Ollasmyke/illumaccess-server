const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

// User Schema
const userSchema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        uniqueCaseInsensitive: true,
        dropDups: true,
        trim: true,
        match: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/
    },
    password: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true,
        default: 'disabled'
    },
    userType: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'UserTypes',
        required: true
    }
}, {
    timestamps: true
});


userSchema.plugin(uniqueValidator);

const Users = mongoose.model('Users', userSchema);

module.exports = Users;
