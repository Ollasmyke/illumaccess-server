const configs = {
    URL: 'http://localhost:8006',
    PROJECT:'Illumaccess-Server',
    ENV:'process.env',
    MONGO_OPTIONS: {
        dbName: 'illumaccess',
        useNewUrlParser: true,
        useUnifiedTopology: true, // Server Discovery and Monitoring engine
        autoIndex: false, // Don't build indexes
        poolSize: 10, // Maintain up to 10 socket connections
        bufferMaxEntries: 0
    },
    VERSION: 'v1',
    STATUS_OPTIONS: {
        ENABLED: 'enabled',
        APPROVED: 'approved',
        PENDING: 'pending',
        DISABLED: 'disabled',
        UNAPPROVED: 'unapproved'
    },


};

module.exports = configs;
