const jwt = require('jsonwebtoken');
const {nanoid} = require('nanoid');
const moment = require('moment');
const config = require('../../constants');
const Token = require('../models/token');

// Authentication Service
const AuthService = {

    checkAuth: (req, res, next) => {
        if (req.headers && req.headers['authorization']) {
            AuthService.verifyJWT(req, res, req.headers['authorization'], (data) => {
                Token.findOne({$and:[{token: data.token}, {userId: data.id}]},  (err, token) => {
                   if (err || !token) {
                       res.status(401).json({
                           message: 'Token not Found!'});
                   } else {
                       if (token.expired) {
                           AuthService.deleteToken(token);
                           res.status(401).json({
                               message: 'Token has expired!'});
                       } else if ( moment().isAfter(moment(token.expireDate))) {
                           AuthService.deleteToken(token);
                           res.status(401).json({
                               message: 'Token has expired!'});
                       } else {
                           AuthService.updateToken(req, res, next, token);
                       }
                   }
                });

            });
        } else {
            res.status(401).json({
                message: 'Authorization Token not Found!'});
        }

    },

    verifyJWT: (req, res, token, cb) => {
        jwt.verify(token, config.SECRET_TOKEN, (error, authData) => {
            if (error) {
                let message = 'Token not found';
                if (error.name === 'TokenExpiredError') {
                    message = 'Token Expired'
                } else if (error.name === 'JsonWebTokenError') {
                    message = 'Invalid Token'
                } else {
                    message = 'Error with Token'
                }
                res.status(401).json({
                    message});
            } else {
                cb (authData);
            }
        })
    },

    deleteToken: (token) => {
        Token.remove({userId: token.userId});
    },

    updateToken: (req, res, next, token) => {
        const hours = moment().add(6, "hours");
        Token.findOneAndUpdate({$and:[{token: token.token}, {userId: token.userId}]}, {$set:{expireDate: hours, expired: false}}, {new: true}, () => {
            // ToDo findOneAndUpdate is deprecated -- get the updated method
            next();
        });

    },

    decodeToken: (req) => {
        return jwt.decode(req.headers['authorization'], process.env.SECRET_TOKEN);
    },

    getValue: (req, key) => {
        return AuthService.decodeToken(req)[key];

    },

    nanoidGenerator: () => {
        return nanoid();
    }

};

module.exports = AuthService;
