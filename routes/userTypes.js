const express = require('express');
const router = express.Router();
const userType = require('../base/controllers/UserTypes');


/* User Types Routes listing. */
router.get('/',  (req, res, next) => {
    userType.getAllUserTypes(req, res);
});

router.post('/',  (req, res, next) => {
    userType.createNewUserType(req, res);
});

router.get('/:id',  (req, res, next) => {
    userType.getUserTypeById(req, res);
});

router.patch('/:id',  (req, res, next) => {
    userType.updateUserType(req, res);
});

router.delete('/:id', (req, res, next) => {
    userType.deleteUserType(req, res)
});


module.exports = router;
