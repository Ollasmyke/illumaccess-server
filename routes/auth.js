const express = require('express');
const router = express.Router();
const auth = require('../base/controllers/Auth.controller');
const authService = require('../base/services/AuthService');

router.post('/login', (req, res) => {
    auth.login(req, res);
});

router.get('/logout', (req, res, next) => {authService.checkAuth(req, res, next)}, (req, res) => {
    auth.logout(req, res);
});

module.exports = router;
