const User = require('../models/user');
const Token = require('../models/token');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const AuthService = require('../services/AuthService');
const PaginateService = require("../services/PaginateService");

const UsersController = {

    getAllUsers: (req, res) => {
        const paginate = PaginateService.validatePageNo(req, res);
        User.countDocuments({}, (err, totalCount) => {
            if (err || !totalCount) {
                res.status(400).json({
                    message: 'Error Occurred fetching all users',
                    error: err
                });
            } else {
                User.find({}, {}, paginate.query, (err, user) => {
                    if (err) {
                        res.status(400).json({
                            message: 'Error Occurred fetching all users',
                            error: err
                        });
                    } else {
                        const pages = Math.ceil(totalCount / paginate.pageSize);
                        res.status(200).json({
                            message: 'Users fetch successfully!',
                            pageNo: paginate.pageNo,
                            pages: pages,
                            user: user
                        });
                    }

                })
            }
        });
    },

    createUser: (req, res) => {
        console.log(req.body);
        User.findOne({email: req.body.email}, (error, data) => {
            if (error) {
                res.status(400).json({
                    message: 'Error occurred, could not even check email address',
                    error: error
                });
            } else {
                if (data) {
                    res.status(401).json({
                        message: 'Email address already exists'
                    });
                } else {
                    const data = req.body;
                    bcrypt.hash(req.body.password, 10, (err, hash) => {
                        if (err) {
                            res.status(401).json({
                                message: 'FAILURE, Password not found!',
                                error: err
                            });
                        } else {
                            let user = new User(data);
                            console.log(req.body);
                            if (req.body.userType === 'STUDENT') {
                                user.userType = process.env.STUDENT_ID;
                                user.status = 'pending';
                            } else if (req.body.userType === 'TUTOR') {
                                user.userType = process.env.STUDENT_ID;
                            } else {
                                user.userType = process.env.ADMIN_ID;
                            }
                            user.password = hash;
                            user.save((err, data) => {
                                if (err) {
                                    res.status(400).json({
                                        message: err.errors[Object.keys(err.errors)[0]].message || 'Error occurred Creating User',
                                        error: err
                                    });
                                } else {
                                    const randomNumber = AuthService.nanoidGenerator();
                                    console.log(user)
                                    const userObject = {
                                        email: user.email,
                                        firstName: user.firstName,
                                        lastName: user.lastName,
                                        fullName: user.firstName + ' ' + user.lastName,
                                        status: user.status,
                                        id: user._id,
                                        userType: user.userType.name,
                                        token: randomNumber
                                    };
                                    Token.create({token: randomNumber, userId: user._id}, () => {
                                        jwt.sign(userObject, process.env.JWT_KEY, {expiresIn: '7d'}, (err, token) => {
                                            res.status(201).json({
                                                message: 'User Created Successfully. Received JSON object to save',
                                                data: {token}
                                            });
                                        })

                                    });
                                }
                            })
                        }
                    });
                }
            }
        })
    },

    getUserById: (req, res) => {
        const id = req.params.id;
        User.findById(id, (err, data) => {
            if (err) {
                res.status(404).json({
                    message: 'Data resource unavailable, wrong ID'
                });
            } else {
                res.status(200).json({
                    message: 'Data found',
                    data: data
                });
            }
        })

    },

    getUserByGroupId: (req, res) => {
        const id = req.params.id;
        User.find({group_id: id})
            .sort({$natural: -1})
            .exec((err, data) => {
                if (err) {
                    res.status(404).json({
                        message: 'Data resource unavailable, wrong ID'
                    });
                } else {
                    res.status(200).json({
                        message: 'Data found',
                        data: data
                    });
                }
            });
    },

    toggleUser: (req, res) => {
        const id = req.params.id;
        User.findById(id, (err, user) => {
            if (err) {
                res.status(404).json({
                    message: 'Data resource unavailable, wrong ID',
                    error: err
                });
            } else {
                user.status = !user.status;
                user.save((err, user) => {
                    if (err) {
                        res.status(400).json({
                            message: 'User status could not be updated',
                            error: err
                        });
                    } else {
                        res.status(201).json({
                            message: 'User status updated successfully.',
                            user: user
                        });
                    }

                });
            }
        })
    },

    updateUser: (req, res) => {
        const id = req.params.id;
        User.findByIdAndUpdate(id, {
            $set: {username: req.body.username, firstname: req.body.firstname}
        }, {new: true}, (err, data) => {
            if (err) {
                res.status(404).json({
                    message: 'Data resource unavailable, wrong ID',
                    error: err
                });
            } else {
                res.status(200).json({
                    message: 'User Group found and updated successfully',
                    data: data
                });
            }
        })
    },

    changePassword: (req, res) => {
        const id = AuthService.getValue(req, 'id');
        const {oldPassword, password} = req.body;
        if (oldPassword === password) {
            res.status(400).json({
                message: 'Password same as old password, change to a new password'
            });
        } else {
            User.findById(id, (err, user) => {
                if (err) {
                    res.status(400).json({
                        message: 'Error occurred, could not get find user to change password',
                        error: err
                    });
                } else {
                    bcrypt.compare(oldPassword, user.password, (err, response) => {
                        if (!response) {
                            res.status(401).json({
                                message: 'FAILURE, Incorrect user password!',
                                error: err
                            });
                        } else {
                            bcrypt.hash(password, 10, (err, hash) => {
                                if (err) {
                                    res.status(401).json({
                                        message: 'FAILURE, Password not found!',
                                        error: err
                                    });
                                } else {
                                    User.findByIdAndUpdate(id, {$set: {password: hash}}, {new: true}, (err, data) => {
                                        if (err) {
                                            res.status(404).json({
                                                message: 'Unable to update Password',
                                                error: err
                                            });
                                        } else {
                                            res.status(200).json({
                                                message: 'User Password updated successfully',
                                                data: data
                                            });
                                        }
                                    })
                                }
                            });
                        }
                    });
                }
            });

        }

    },

    forgotPassword: () => {

    },

    deleteUser: (req, res) => {
        const id = req.params.id;
        User.remove(id, (err, data) => {
            if (err) {
                res.status(404).json({
                    message: 'Data resource unavailable, wrong ID',
                    error: err,
                });
            } else {
                res.status(200).json({
                    message: 'User deleted successfully',
                    data: data
                });
            }
        });
    }

};

module.exports = UsersController;
